# Ansible Role: Elasticsearch Curator

An Ansible Role that installs Elasticsearch Curator on RedHat/CentOS Linux.
It is installed into system on the box.

## Requirements
virtualenv must be installed on the system.
- curator_user - Elasticsearch Curator user for which will be installed and configured. By default it is set to 'curator'.
- curator_group - Elasticsearch Curator group. By default it is set to 'curator'.
- curator_actions - List with configuration for action file.
- curator_excluded_index - List with exluded index.
- elasticsearch_nodes - List with Elasticsearch nodes for which curator should administrate indices. By default it is set to [127.0.0.1].
- elasticsearch_port - Elasticsearch port. By default it is set to 9200.

## Optional
- curator_version - Elasticsearch Curator version which should be installed. By default it is set to 4.0.5.
- curator_log_dir - Elasticsearch Curator log path. By default it is set to '/var/log/elasticsearch-curator'.
- curator_log_file - Elasticsearch Curator log filename. By default it is set to 'elasticsearch-curator.log'.
- curator_conf_dir - Directory contains config and action files. By default it is set to '/etc/elasticsearch-curator'.
- curator_venv_dir - Path to virtualenv. By default it is set to /opt/elasticsearch-curator.
- curator_path - Path to curator CLI. By default it is set to /opt/elasticsearch-curator/bin/curator
- curator_job - Name of the job that will be defined as a Cron job. By default it is set to 'Elasticsearch Curator jobs'.
- curator_cron_config - List with configuration for Cron daemon.
- run_mode: One of Deploy, Stop, Install, Start, or Use. The default is Deploy which will do Install, Configure, then Start.
- skip_install - Skip install. By default it is set to 'False'.
- curator_log_level - Log level for Curator. By default it is set to 'WARN'.
- curator_master_only - Run jobs only on Elasticsearch master node.
- curator_install_user_group - Should role create user (```curator_user```) & group (```curator_group```)

## Configuration
To configure Curator action modify curator_actions list. Example configuration for deleting indices by age:
```
curator_actions:
 - {
      delete_by: age,
      description: 'Delete indices older than 7 days',
      deleted_days: 7,
      disable: False
    }
```
Example configuration for deleting indices by size. Size should be in MB.
```
curator_actions:
- {
    delete_by: size,
    description: 'Delete indices >= 1000 MB',
    deleted_size: 1000,
    disable: False
  }
```
Example configuration for exclude index which should not be removed:
```
curator_excluded_index:
- {
    index_name: .kibana
    exclude: True
  }
```
Configure Cron daemon for executing jobs is made by `curator_cron_config` variable. Example configuration to run job every day at 00:00 :
```
curator_cron_config:
  - {
      minute: 0,
      hour: 0
    }
```
Available parameters are:
 - minute
 - hour
 - day
 - weekday
 - month

## License
Apache License, Version 2.0

## Author Information
artur.basiak@ts.fujitsu.com
